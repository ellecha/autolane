import React, { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom'

function AppointmentForm() {
  const [vin, setVin] = useState('');
  const [technicians, setTechnicians] = useState([]);
  const [technician, setTechnician] = useState('');
  const [customer, setCustomer] = useState('');
  const [dateTime, setDateTime] = useState('');
  const [reason, setReason] = useState('');
  const [sales, setSales] = useState([]);
  const navigate = useNavigate()

  const fetchData = async() => {
    const url ='http://localhost:8080/api/technicians/'

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data["technicians"])
    }
  }

  function handleVinChange(event) {
    setVin(event.target.value);
  }

  function handleTechnicianChange(event) {
    setTechnician(event.target.value);
  }

  function handleCustomerChange(event) {
    setCustomer(event.target.value);
  }

  function handleDateTimeChange(event) {
    setDateTime(event.target.value);
  }

  function handleReasonChange(event) {
    setReason(event.target.value);
  }

  const fetchSales = async () => {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    } else {
      console.log('Could not fetch sales:', response.statusText);
    }

  }


  useEffect(() => {
    fetchData();
    fetchSales();
  }, []);

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      vin: vin,
      technician: technician,
      customer: customer,
      date_time: dateTime,
      reason: reason,
    };
    console.log(data);

    const technicianUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      console.log(newAppointment);
      setVin('');
      setTechnician('');
      setCustomer('');
      setDateTime('');
      setReason('');
    }
    navigate("/appointments")
  };

  const isVinSold = sales.some(sale => sale.automobile.vin === vin);
  const isVIP = isVinSold ? 'VIP' : '';

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Make an appointment</h1>
          <form onSubmit={handleSubmit} id="add-an-appointment-form">
            <div className="form-floating mb-3">
              <input value={vin} onChange={handleVinChange} placeholder="VIN" type="text" name="vin" className="form-control" />
              <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input value={customer} onChange={handleCustomerChange} placeholder="Customer" type="text" name="customer" id="customer" className="form-control" />
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input value={dateTime} onChange={handleDateTimeChange} placeholder="Date and Time" type="datetime-local" name="date_time" id="date_time" className="form-control" />
              <label htmlFor="date_time">Date and Time</label>
            </div>
            <div className="form-floating mb-3">
              <input value={reason} onChange={handleReasonChange} placeholder="Reason" type="text" name="reason" id="reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
            </div>
            <div className="form-floating mb-3">
              <select value={technician} onChange={handleTechnicianChange} name="technician" id="technician" className="form-control">
                <option value="">Choose a technician</option>
                {technicians.map((technician) => (
                  <option key={technician.employee_id} value={technician.employee_id}>
                    {technician.first_name} {technician.last_name}
                  </option>
                ))}
              </select>
              <label htmlFor="technician">Technician</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
      {isVinSold && (
        <div className="offset-3 col-6 mt-3">
          <div className="alert alert-warning" role="alert">
            VIN {vin} is associated with a sale. Customer is {isVIP}.
          </div>
        </div>
      )}
    </div>
  );
}

export default AppointmentForm;
