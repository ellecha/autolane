import React, { useState } from "react";

function SalesForm({automobiles, salespeople, customers, getSales}) {
    const [price, setPrice] = useState('');
    const [automobile, setAutomobile] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        const data  = {
            price: price,
            automobile: automobile,
            salesperson: salesperson,
            customer: customer,
        };

        const saleUrl = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
              },
        };

        const response = await fetch(saleUrl, fetchConfig)
        if (response.ok) {
            setPrice('');
            setAutomobile('');
            setSalesperson('');
            setCustomer('');

            getSales();
            window.location.href = 'http://localhost:3000/sales';
        }
    }

    function handleChangePrice(event) {
        const { value } = event.target;
        setPrice(value);
    }

    function handleChangeAutomobile(event) {
        const { value } = event.target;
        setAutomobile(value);
    }

    function handleChangeSalesperson(event) {
        const { value } = event.target;
        setSalesperson(value);
    }

    function handleChangeCustomer(event) {
        const { value } = event.target;
        setCustomer(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-5 mt-5">
                <h1>Create New Sale</h1><br></br>
                <form onSubmit={handleSubmit} id="create-sale-form">
                    <div className="form-floating mb-3">
                        <input value={price} onChange={handleChangePrice} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                        <label htmlFor="price">Price</label>
                    </div>
                        <div className="mb-3">
                            <select value={automobile} onChange={handleChangeAutomobile} required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose automobile</option>
                                {automobiles.map(automobile => {
                                    if (automobile.sold === false) {
                                return (
                                    <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                )
                                    }
                                })}
                            </select>
                        </div>
                              <div className="mb-3">
                            <select value={salesperson} onChange={handleChangeSalesperson} required name="salesperson" id="salesperson" className="form-select">
                                <option value="">Choose salesperson</option>
                                {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                                )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={customer} onChange={handleChangeCustomer} required name="customer" id="customer" className="form-select">
                                <option value="">Choose customer</option>
                                {customers.map(customer => {
                                return (
                                    <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                )
                                })}
                            </select>
                        </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
    );
}

export default SalesForm;
