import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AutomobileForm from './AutomobileForm';
import AutomobilesList from './AutomobilesList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import MainPage from './MainPage';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import ModelForm from './ModelForm';
import ModelList from './ModelList';
import Nav from './Nav';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalespersonForm from './SalespersonForm';
import SalespeopleList from './SalespeopleList';
import ServiceHistory from './ServiceHistory';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';

function App() {

  const [automobiles, setAutomobiles] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [models, setModels] = useState([]);
  const [sales, setSales] = useState([]);
  const [salespeople, setSalespeople] = useState([]);

  async function getAutomobiles() {
    const url = 'http://localhost:8100/api/automobiles/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos)
    }
  }

  async function getCustomers() {
    const url = 'http://localhost:8090/api/customers/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers)
    }
  }

  async function getManufacturers() {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }

  async function getModels() {
    const url = 'http://localhost:8100/api/models/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      setModels(data.models)
    }
  }

  async function getSales() {
    const url = 'http://localhost:8090/api/sales/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales)
    }
  }

  async function getSalespeople() {
    const url = 'http://localhost:8090/api/salespeople/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople)
    }
  }

  useEffect(() => {
    getAutomobiles();
    getCustomers();
    getManufacturers();
    getModels();
    getSales();
    getSalespeople();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/technicians/create" element={<TechnicianForm />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/create" element={<AppointmentForm />} />
          <Route path="/servicehistory" element={<ServiceHistory />} />
          <Route path="/automobiles" element={<AutomobilesList automobiles={automobiles} />} />
          <Route path="/automobiles/create" element={<AutomobileForm getAutomobiles={getAutomobiles} />} />
          <Route path="/customers" element={<CustomerList customers={customers} />} />
          <Route path="/customers/create" element={<CustomerForm getCustomers={getCustomers} />} />
          <Route path="/manufacturers" element={<ManufacturerList manufacturers={manufacturers} />} />
          <Route path="/manufacturers/create" element={<ManufacturerForm getManufacturers={getManufacturers} />} />
          <Route path="/models" element={<ModelList models={models} />} />
          <Route path="/models/create" element={<ModelForm getModels={getModels} />} />
          <Route path="/sales" element={<SalesList sales={sales} />} />
          <Route path="/sales/create" element={<SalesForm automobiles={automobiles} salespeople={salespeople} customers={customers} getSales={getSales} />} />
          <Route path="/salespeople" element={<SalespeopleList salespeople={salespeople} />} />
          <Route path="/salespeople/create" element={<SalespersonForm getSalespeople={getSalespeople} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
