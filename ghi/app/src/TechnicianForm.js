import React, { useState } from "react";
import { useNavigate } from 'react-router-dom'


function TechnicianForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');
    const navigate = useNavigate()

    function handleFirstNameChange(event) {
        setFirstName(event.target.value);
    };

    function handleLastNameChange(event) {
        setLastName(event.target.value);

    };

    function handleEmployeeIdChange(event) {
        setEmployeeId(event.target.value);
    };

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
        first_name: firstName,
        last_name: lastName,
        employee_id: employeeId,
        };
        console.log(data);


        const technicianUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            console.log(newTechnician);
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }
        navigate("/technicians")
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a technician</h1>
                    <form onSubmit={handleSubmit} id="add-a-technician-form">
                        <div className="form-floating mb-3">
                            <input value={firstName} onChange={handleFirstNameChange} placeholder="First name" type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="firstName">First name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={lastName} onChange={handleLastNameChange} placeholder="Last name" type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="lastName">Last name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={employeeId} onChange={handleEmployeeIdChange} placeholder="Employee ID" type="text" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="employee_Id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;
