import React, { useState, useEffect } from 'react';

function ServiceHistoryList() {
  const [appointments, setAppointments] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);
  const [vin, setVin] = useState('');

  async function fetchAppointments() {
    try {
      const response = await fetch("http://localhost:8080/api/appointments/");
      if (response.ok) {
        const data = await response.json();
        console.log(data);
        setAppointments(data.appointments);
      }
    } catch (error) {
      console.error("Could not fetch technician:", error);
    }
  }

  async function fetchAutomobiles() {
    try {
      const secondResponse = await fetch(`http://localhost:8100/api/automobiles/`);
      console.log(secondResponse);
      if (secondResponse.ok) {
        const data = await secondResponse.json();
        setAutomobiles(data.autos);
      }
    } catch (error) {
      console.error("Could not fetch automobile:", error);
    }
  }

  useEffect(() => {
    fetchAppointments();
  }, []);

  useEffect(() => {
    fetchAutomobiles();
  }, []);

  function handleVinChange(event) {
    setVin(event.target.value);
  }

  function filterAppointments() {
    const filteredAppointments = appointments.filter(appointment => appointment.vin === vin);
    setAppointments(filteredAppointments);
  }

  function soldStatus(vin) {
    for (let auto of automobiles) {
      if (vin === auto["vin"]) {
        return "Yes";
      }
    }
    return "No";
  }

  function formatDate(string) {
    const date = new Date(string);
    const timeString = date.toLocaleTimeString(undefined, { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: '2-digit' });
    return timeString.replace(/\s/g, '').replace(',', ', ');
  }

  return (
    <div>
      <h1 style={{ marginTop: '10px' }}>Service History</h1>
      <div className="form-floating mb-3">
        <input value={vin} onChange={handleVinChange} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
        <label htmlFor="vin">Search by VIN</label>
        <button onClick={() => filterAppointments()} className="btn btn-primary">Search</button>
      </div>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer Name</th>
            <th>Date and Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{soldStatus(appointment.vin)}</td>
                <td>{appointment.customer}</td>
                <td>{formatDate(appointment.date_time)}</td>
                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistoryList;
