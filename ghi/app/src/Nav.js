import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <div className="dropdown">
          <button className="btn btn-light dropdown-toggle me-2" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Inventory
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/">Manufacturers</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/create/">Create Manufacturer</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/models/">Models</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/models/create/">Create Model</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/automobiles/">Automobiles</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/automobiles/create/">Create Automobile</NavLink>
            </div>
            <button className="btn btn-light dropdown-toggle me-2" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Sales
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <NavLink className="dropdown-item" aria-current="page" to="/salespeople/"> Salespeople</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/salespeople/create/"> Add Salesperson</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/customers/"> Customers</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/customers/create/"> Add Customer</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/sales/"> Sales</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/sales/create/"> Add Sale</NavLink>
            </div>
            <button className="btn btn-light dropdown-toggle me-2" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Services
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <NavLink className="dropdown-item" aria-current="page" to="/technicians/">Technicians</NavLink>
            <NavLink className="dropdown-item" aria-current="page" to="/technicians/create/">Create Technician</NavLink>
            <div className='dropdown-divider'></div>
            <NavLink className="dropdown-item" aria-current="page" to="/appointments/">Appointments</NavLink>
            <NavLink className="dropdown-item" aria-current="page" to="/appointments/create/">Create Appointment</NavLink>
            <div className='dropdown-divider'></div>
            <NavLink className="dropdown-item" aria-current="page" to="/servicehistory">Service History</NavLink>
            </div>
            {/* <NavLink className="btn btn-light me-2" style={{color:"black"}} aria-current="page" to="/contact/">Contact Us</NavLink> */}
        </div>
      </div>
    </nav>
  )
}

export default Nav;
